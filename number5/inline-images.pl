#!/usr/bin/env perl

use warnings;
use strict;

use MIME::Base64 qw< encode_base64 >;

sub slurp {
    open my $fh, "<", $_[0] or die $!;
    local $/;
    return scalar <$fh>;
}

while(<>) {
    s#<img src="(.*?)"#
        "<img src=\"data:@{[`file --mime-type -b \Q$1\E | tr -d '\n'`]};base64,@{[join '', split \"\n\", encode_base64(slurp($1))]}\"";
    #eg;
    print;
}
