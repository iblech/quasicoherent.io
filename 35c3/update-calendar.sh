#!/usr/bin/env bash

fahrplan="$(curl "https://fahrplan.events.ccc.de/congress/2018/Fahrplan/schedule.ics")"
sessions="$(curl "https://events.ccc.de/congress/2018/wiki/index.php/Special:Ask/cl:YzpdjssOgyAQRb-IYLBuTNjYR7rvF4w4xmlBDIxN_fui1miaQHLgnpsZUVSiqO4Qhcp8_UTDCXgasCyvb-xZFJf5qEwcRFyS5AUwr7J8YIzk-1WVIr_9WcQWdRydgzDtcWQIa-xSOj8O1b7ZkoSHzm-Syqw3wAn1Brs0BqvT3T8ajCbQsOgHlg6ot1Cj1dKSI9YqPynp2zYi60y2PjhgTQZsWgKCjAjBdGvj-hn8un5H84xNSrjMpDPYLw")"

[ ${#fahrplan} -ge 1000 ] || exit 1
[ ${#sessions} -ge 1000 ] || exit 1

{
    echo "$fahrplan" | sed -e '$d' -e '2iX-WR-CALNAME:35C3 Fahrplan+Wiki'
    echo "$sessions" | sed -e '1,/VEVENT/{/VEVENT/p;d}'
} > "$(dirname "$BASH_SOURCE")/cal.ics"
